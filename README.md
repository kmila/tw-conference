# tw-conference: Problem 2

Proof-of-concept application developed by Camila Coelho for TW.

### Compile ###

* Requires Java 8
* Build via maven: `mvn clean install`
* Create an input file with the session/duration list
* Run as executable jar: `java -jar target/conference*.jar INPUT_FILE`

Example:

`java -jar target/conference-0.0.1-SNAPSHOT.jar src/test/resources/tracks.txt`

### Sample output ###

```
Generated 2 tracks

Track-0
9:00AM Writing Fast Tests Against Enterprise Rails 60min
10:00AM Overdoing it in Python 45min
10:45AM Lua for the Masses 30min
11:15AM Ruby Errors from Mismatched Gem Versions 45min
12:00PM Lunch
1:00PM Common Ruby Errors 45min
1:45PM Rails for Python Developers lightning
1:50PM Communicating Over Distance 60min
2:50PM Accounting-Driven Development 45min
3:35PM Woah 30min
4:05PM Sit Down and Write 30min

Track-1
9:00AM Pair Programming vs Noise 45min
9:45AM Rails Magic 60min
10:45AM Ruby on Rails: Why We Should Move On 60min
11:45AM Clojure Ate Scala (on my project) 45min
12:00PM Lunch
1:00PM Programming in the Boondocks of Seattle 30min
1:30PM Ruby vs. Clojure for Back-End Development 30min
2:00PM Ruby on Rails Legacy App Maintenance 60min
3:00PM A World Without HackerNews 30min
3:30PM User Interface CSS in Rails Apps 30min
```

### Known issues ###

* Insufficient test coverage
* Bug on session before lunch
* Non-optimal solution (sequential)