package br.tw.com.conference.util;

import br.tw.com.conference.exception.CommandLineException;
import java.nio.file.Paths;
import org.junit.Test;

public class FileUtilTest {

  @Test
  public void validateInput() throws Exception {
    FileUtil.validateInput(Paths.get(getClass().getResource("/tracks.txt").toURI()));
  }

  @Test(expected = CommandLineException.class)
  public void validateNull() {
    FileUtil.validateInput(null);
  }

  @Test(expected = CommandLineException.class)
  public void validateEmpty() {
    FileUtil.validateInput(Paths.get(""));
  }

}