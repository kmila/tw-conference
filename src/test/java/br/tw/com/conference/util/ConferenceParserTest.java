package br.tw.com.conference.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ConferenceParserTest {

  private static final String SAMPLE_STRING = "Lua for the Masses";

  @Mock
  private BufferedReader mockReader;

  @Mock
  private BufferedWriter mockWriter;

  @Spy
  private ConferenceParser conferenceParser;

  private List<String> result;

  @Before
  public void setUp() {
    result = new ArrayList<>();
    BiConsumer<BufferedWriter, String> mockConsumer = (bufferedWriter, s) -> {
      result.add(s);
    };
    Mockito.doReturn(mockReader).when(conferenceParser).getReader(any());
    Mockito.doReturn(mockWriter).when(conferenceParser).addWriter(any());
    Mockito.doReturn(mockConsumer).when(conferenceParser).writeLineFn();
  }

  @Test
  public void testParseNothing() {
    conferenceParser.parse("");
  }

  @Test
  public void testParse() throws Exception {
    List<String> mockLines = Collections.singletonList(SAMPLE_STRING + " 30min");
    Mockito.when(mockReader.readLine()).thenReturn(mockLines.get(0), null);

    conferenceParser.parse("");
    assertEquals(("9:00AM " + SAMPLE_STRING + " 30min"), result.get(0));
  }

  @Test
  public void testParseLightning() throws Exception {
    List<String> mockLines = Collections.singletonList(SAMPLE_STRING + " lightning");
    Mockito.when(mockReader.readLine()).thenReturn(mockLines.get(0), null);

    conferenceParser.parse("");
    assertEquals(("9:00AM " + SAMPLE_STRING + " lightning"), result.get(0));
  }

}