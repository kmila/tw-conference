package br.tw.com.conference.exception;

import org.springframework.boot.ExitCodeGenerator;

public class CommandLineException extends RuntimeException implements ExitCodeGenerator {

  public CommandLineException(String message, Throwable cause) {
    super(message, cause);
  }

  @Override
  public int getExitCode() {
    return -1;
  }
}
