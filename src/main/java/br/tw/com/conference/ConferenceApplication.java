package br.tw.com.conference;

import br.tw.com.conference.exception.CommandLineException;
import br.tw.com.conference.util.ConferenceParser;
import br.tw.com.conference.util.FileUtil;
import java.io.BufferedReader;
import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Scanner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConferenceApplication implements CommandLineRunner {

  public static final String TRACKS_FILE = "_tracks";

  private static final String DEBUG = "debug";

  @Autowired
  private ConferenceParser conferenceParser;

  public static void main(String[] args) {
    SpringApplication.run(ConferenceApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    if (args == null || args.length == 0) {
      System.err.println("Usage: app.jar <conference_file_path> [-debug]");
      return;
    }
    String conferenceTracks = args[0];
    boolean isDebug = isDebug(args);
    try {
      int results = conferenceParser.parse(conferenceTracks);
      System.out.println("Generated " + results + " tracks");
      FileUtil.print(isDebug);
    } catch (CommandLineException e) {
      if (isDebug) {
        e.printStackTrace();
      }
      System.err.println("Error processing " + conferenceTracks + ":" + e.getMessage());
      System.exit(e.getExitCode());
    }
  }

  private static boolean isDebug(String[] args) {
    return Arrays.stream(args).anyMatch(a -> DEBUG.equalsIgnoreCase(a));
  }

}
