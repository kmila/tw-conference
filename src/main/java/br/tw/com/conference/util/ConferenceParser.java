package br.tw.com.conference.util;

import static java.time.temporal.ChronoUnit.MINUTES;

import br.tw.com.conference.exception.CommandLineException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class ConferenceParser {

  protected static final Pattern DURATION_PATTERN = Pattern.compile(".*?(\\d+)min");
  protected static final String LIGHTNING = "lightning";
  protected static final int LIGHTNING_DURATION = 5;
  protected static final String LUNCH_TIME = "12:00PM Lunch";
  protected static final DateTimeFormatter TIME_FMT = DateTimeFormatter.ofPattern("h:mma");

  public int parse(String input) {
    List<BufferedWriter> tracks = new ArrayList<>();
    try (BufferedReader br = getReader(input)) {

      BiConsumer writer = writeLineFn();

      Duration remaining = Duration.ofHours(3);
      Duration startTime = Duration.ofHours(9);

      BufferedWriter bw = addWriter(tracks);
      boolean isNetworking = false;

      String line;
      while ((line = br.readLine()) != null) {

        if (line.isEmpty()) {
          continue;
        }

        Duration session = Duration.of(parseDuration(line), MINUTES);
        remaining = remaining.minus(session);

        if (remaining.isNegative()) {
          remaining = Duration.ofHours(3);

          if (isNetworking) {
            isNetworking = false;
            startTime = Duration.ofHours(9);
            bw = addWriter(tracks);
          } else {
            isNetworking = true;
            writer.accept(bw, LUNCH_TIME);
            startTime = Duration.ofHours(13);

          }
        }

        writer.accept(bw, parseLine(line, startTime));
        startTime = startTime.plus(session);

      }

    } catch (Exception e) {
      throw new CommandLineException(e.getMessage(), e);
    } finally {
      for (BufferedWriter w : tracks) {
        try {
          w.close();
        } catch (IOException e) {
          throw new CommandLineException(e.getMessage(), e);
        }
      }
    }
    return tracks.size();
  }

  private static String parseLine(String line, Duration startTime) {
    return TIME_FMT.format(LocalTime.ofSecondOfDay(startTime.getSeconds())) + " " + line;
  }

  private int parseDuration(String line) {
    Matcher m = DURATION_PATTERN.matcher(line);
    if (m.matches()) {
      return Integer.parseInt(m.group(1));
    }
    return line.endsWith(LIGHTNING) ? LIGHTNING_DURATION : 0;
  }

  protected BiConsumer<BufferedWriter, String> writeLineFn() {
    return (bw, s) -> {
      try {
        bw.append(s);
        bw.newLine();
      } catch (Exception e) {
        throw new CommandLineException(e.getMessage(), e);
      }
    };
  }

  protected BufferedWriter addWriter(List<BufferedWriter> tracks) {
    BufferedWriter writer = FileUtil.writer(tracks.size());
    tracks.add(writer);
    return writer;
  }

  protected BufferedReader getReader(String input) {
    return FileUtil.reader(Paths.get(input));
  }
}
