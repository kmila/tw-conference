package br.tw.com.conference.util;

import br.tw.com.conference.exception.CommandLineException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileUtil {

  public static final String TRACK_DIR = "_trackdir";

  public static final String TRACK_FILE = "Track-%d_";

  private static final Path TEMP_DIRECTORY = createTempDir();

  public static Path createTempDir() {
    try {
      return Files.createTempDirectory(TRACK_DIR);
    } catch (Exception e) {
      throw new CommandLineException(e.getMessage(), e);
    }
  }

  public static void validateInput(Path input) {
    if (input == null || !input.toFile().isFile()) {
      throw new CommandLineException(input + " not found", new IllegalArgumentException());
    }
  }

  public static BufferedReader reader(Path input) {
    validateInput(input);
    try {
      return Files.newBufferedReader(input);
    } catch (Exception e) {
      throw new CommandLineException(e.getMessage(), e);
    }
  }

  public static BufferedWriter writer(int track) {
    try {
      File tmpFile = Files.createTempFile(TEMP_DIRECTORY, String.format(TRACK_FILE, track), null).toFile();
      return new BufferedWriter(new FileWriter(tmpFile, true));
    } catch (Exception e) {
      throw new CommandLineException(e.getMessage(), e);
    }
  }

  public static void print(boolean isDebug) {
    try {
      Files.list(TEMP_DIRECTORY).sorted().forEach(f -> print(f, isDebug));
    } catch (Exception e) {
      throw new CommandLineException(e.getMessage(), e);
    }
    if (!isDebug) {
      TEMP_DIRECTORY.toFile().deleteOnExit();
    }
  }

  public static void print(Path file, boolean isDebug) {
    System.out.println("\n" + String.valueOf(file.getFileName()).split("_")[0]);

    try (BufferedReader br = FileUtil.reader(file)) {
      String line;
      while ((line = br.readLine()) != null) {
        System.out.println(line);
      }
    } catch (Exception e) {
      throw new CommandLineException(String.valueOf(file), e);
    }
    if (!isDebug) {
      file.toFile().deleteOnExit();
    }
  }

}
